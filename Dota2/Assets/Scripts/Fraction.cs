﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum charFraction
{
    red,
    blue,
    neutral,
    land
}
public class Fraction : MonoBehaviour
{
    public charFraction userFraction;

    public Material red;
    public Material blue;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void setFraction(charFraction fraction)
    {
        userFraction = fraction;
    }

    public void setFractionColor()
    {
        MeshRenderer meshRenderer = gameObject.GetComponent<MeshRenderer>();
        if (userFraction == charFraction.red)
        {
            meshRenderer.materials[0] = red;
        }

        if (userFraction == charFraction.blue)
        {
            meshRenderer.materials[0] = blue;
        }
    }
}
