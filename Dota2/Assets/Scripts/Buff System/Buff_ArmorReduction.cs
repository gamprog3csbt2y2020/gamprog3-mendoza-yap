﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Buff/Armor Reduction")]
public class Buff_ArmorReduction : Buff
{
    public float Reduction = 1;
    protected override void OnBuffStart(BuffReceiver target)
    {
        target.Stats.armor -= Reduction;
    }

    protected override void OnBuffEnd(BuffReceiver target)
    {
        target.Stats.armor += Reduction;
    }
}
