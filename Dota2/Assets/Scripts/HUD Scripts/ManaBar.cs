﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaBar : MonoBehaviour
{
    public Mana manaRef;

    [SerializeField] private Image currentManaImg;
    private Text text;

    private void Start()
    {
        text = GetComponentInChildren<Text>();
    }

    void Update()
    {
        currentManaImg.fillAmount = Mathf.Clamp(manaRef.GetNormalizedMana(), 0, 1);
        text.text = manaRef.CurrentMana + "/" + manaRef.MaxMana;
    }
}
