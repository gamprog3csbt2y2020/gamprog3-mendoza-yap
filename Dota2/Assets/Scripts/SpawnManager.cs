﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    //This manages all spawners

    public GameObject[] generalCreepSpawn; //general creeps
    public GameObject[] siegeCreepSpawn; //siege creep
    public GameObject[] superCreepSpawn; //super creep spawn
    public GameObject[] superSiegeCreepSpawn; //super creep siege spawn

    public float spawnInterval = 30;
    public bool activateSecondLevel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
