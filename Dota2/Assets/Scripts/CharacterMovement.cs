﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class CharacterMovement : MonoBehaviour
{
    public NavMeshAgent playerAgent;

    public GameObject player;

    public GameObject movementIndicator;
    public GameObject enemyTarget;

    public Vector3 characterTarget;

    public bool moving;
    public bool attacking;


    // Start is called before the first frame update
    void Start()
    {
        playerAgent = this.GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {

        if (enemyTarget != null)
        {
            playerAgent.SetDestination(enemyTarget.gameObject.transform.position);
        }

        if (Input.GetButtonDown("Fire2"))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            Physics.Raycast(ray, out hit);

            
            if (hit.collider.gameObject.tag == "creep" && hit.collider.GetComponent<Fraction>() != null && this.gameObject.GetComponent<Fraction>().userFraction != hit.collider.GetComponent<Fraction>().userFraction)
            {
                Debug.Log("Enemy Selected");
               movementIndicator.SetActive(true);
               enemyTarget = hit.collider.gameObject;
                movementIndicator.GetComponent<Rigidbody>().position = new Vector3(enemyTarget.gameObject.transform.position.x, 0.0f, enemyTarget.gameObject.transform.position.z);
               playerAgent.SetDestination(new Vector3(enemyTarget.gameObject.transform.position.x, 0.0f, enemyTarget.gameObject.transform.position.z));
            }
            else if (hit.collider.gameObject.tag == "tower" && hit.collider.GetComponent<Fraction>() != null && this.gameObject.GetComponent<Fraction>().userFraction != hit.collider.GetComponent<Fraction>().userFraction)
            {

                    Debug.Log("Enemy Selected");
                    movementIndicator.SetActive(true);
                    enemyTarget = hit.collider.gameObject;
                    movementIndicator.GetComponent<Rigidbody>().position = new Vector3(enemyTarget.gameObject.transform.position.x, 0.0f, enemyTarget.gameObject.transform.position.z);
                    playerAgent.SetDestination( new Vector3(enemyTarget.gameObject.transform.position.x,0.0f, enemyTarget.gameObject.transform.position.z));

            }
            else
            {
                movementIndicator.SetActive(true);
                enemyTarget = null;
                movementIndicator.GetComponent<Rigidbody>().position = new Vector3(hit.point.x, 0.00f, hit.point.z);
                playerAgent.SetDestination(new Vector3(hit.point.x,0.00f, hit.point.z));
            }

            Debug.Log(hit.point);
        }

        if (playerAgent.remainingDistance >= playerAgent.stoppingDistance)
        {
            moving = true;
        }
        else
        {
            movementIndicator.SetActive(false);
            moving = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "creep")
        {
            Debug.Log("Enemy Spotted");
        }
        if (other.gameObject == enemyTarget.gameObject)
        {
            Debug.Log("Attacking: " + other.gameObject.name);
            //start attacking
            this.GetComponent<AttackComponent>().Attack(enemyTarget.GetComponent<Health>());
        }
    }
}
