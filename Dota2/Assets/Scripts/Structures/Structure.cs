﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Structure : MonoBehaviour
{
    public bool Invulnerable { get => invulnerable; }
    [SerializeField] private bool invulnerable;

    protected Health health;

    public void RemoveVulnerability()
    {
        invulnerable = false;
    }
}
