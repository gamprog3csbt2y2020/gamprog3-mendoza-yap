﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gold : MonoBehaviour
{
    public int gold;
    public int releaseingGold; //this releases the gold to target


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //use this after defeat
    void gainGold(int amount)
    {
        gold += amount;
    }

    //use this on a market script
    public bool spendGold(int payment)
    {
        if (gold <= payment)
        {
            Debug.Log("Can't purchase item");
            return false;
        }
        else
        {
            return true;
        }
    }
}
