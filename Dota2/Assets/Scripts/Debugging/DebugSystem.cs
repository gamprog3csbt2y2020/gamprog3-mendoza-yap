﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum SpeedSelection
{
    slow,
    normal,
    fast,
    veryFast
}

public class DebugSystem : MonoBehaviour
{
    public Text speedUIText;

    public GameObject debugUIObject;
    public bool debugUIActivate;
    public float speedControl;

    public SpeedSelection setSpeed;

    public GameObject[] redTowerTop;
    public GameObject[] redTowerMid;
    public GameObject[] redTowerBot;
    public GameObject[] blueTowerTop;
    public GameObject[] blueTowerMid;
    public GameObject[] blueTowerBot;

    public GameObject[] redTowerfountain;
    public GameObject[] blueTowerfountain;

    public GameObject[] redBarracks;
    public GameObject[] blueBarracks;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        speedUIText.text = "Speed: " + setSpeed.ToString();
    }

    public void DecreaseSpeed()
    {
        switch (setSpeed)
        {
            case SpeedSelection.slow:
                Debug.Log("Cannot decrease Speed");
                break;
            case SpeedSelection.normal:
                setSpeed = SpeedSelection.slow;
                break;
            case SpeedSelection.fast:
                setSpeed = SpeedSelection.normal;
                break;
            case SpeedSelection.veryFast:
                setSpeed = SpeedSelection.fast;
                break;
            default:
                break;
        }

        ChangeSpeed();
    }

    public void IncreaseSpeed()
    {
        switch (setSpeed)
        {
            case SpeedSelection.slow:
                setSpeed = SpeedSelection.normal;
                break;
            case SpeedSelection.normal:
                setSpeed = SpeedSelection.fast;
                break;
            case SpeedSelection.fast:
                setSpeed = SpeedSelection.veryFast;
                break;
            case SpeedSelection.veryFast:
                Debug.Log("Cannot increase Speed");
                break;
            default:
                break;
        }

        ChangeSpeed();
    }

    public void DestroyTower(GameObject tower)
    {
        tower.GetComponent<Health>().Damage(999999999999);
    }

    void ChangeSpeed()
    {

        switch (setSpeed)
        {
            case SpeedSelection.slow:
                speedControl = 0.25f;
                break;
            case SpeedSelection.normal:
                speedControl = 1.0f;
                break;
            case SpeedSelection.fast:
                speedControl = 2.0f;
                break;
            case SpeedSelection.veryFast:
                speedControl = 4.0f;
                break;
            default:
                break;
        }
        Time.timeScale = speedControl;
    }

    public void EnableDebugUI()
    {
        if (debugUIActivate == true)
        {
            debugUIActivate = false;
        }
        else
        {
            debugUIActivate = true;
        }
        debugUIObject.SetActive(debugUIActivate);
    }

    public void destroyRedTop()
    {
        for (int i = 0; i < redTowerTop.Length; i++)
        {
            if (redTowerTop[i] != null)
            {
                Debug.Log(redTowerTop[i].name + "is destroyed!");
                redTowerTop[i].GetComponent<Health>().Damage(999999999999999999);
                break;
            }
        }
    }

    public void destroyRedBottom()
    {
        for (int i = 0; i < redTowerBot.Length; i++)
        {
            if (redTowerBot[i] != null)
            {
                Debug.Log(redTowerBot[i].name + "is destroyed!");
                redTowerBot[i].GetComponent<Health>().Damage(999999999999999999);
                break;
            }
        }
    }

    public void destroyRedMid()
    {
        for (int i = 0; i < redTowerMid.Length; i++)
        {
            if (redTowerTop[i] != null)
            {
                Debug.Log(redTowerMid[i].name + "is destroyed!");
                redTowerMid[i].GetComponent<Health>().Damage(999999999999999999);
                break;
            }
        }
    }

    public void destroyBlueTop()
    {
        for (int i = 0; i < blueTowerTop.Length; i++)
        {
            if (blueTowerTop[i] != null)
            {
                Debug.Log(blueTowerTop[i].name + "is destroyed!");
                blueTowerTop[i].GetComponent<Health>().Damage(999999999999999999);
                break;
            }
        }
    }

    public void destroyBlueBottom()
    {
        for (int i = 0; i < blueTowerBot.Length; i++)
        {
            if (redTowerBot[i] != null)
            {
                Debug.Log(blueTowerBot[i].name + "is destroyed!");
                blueTowerBot[i].GetComponent<Health>().Damage(999999999999999999);
                break;
            }
        }
    }

    public void destroyBlueMid()
    {
        for (int i = 0; i < blueTowerMid.Length; i++)
        {
            if (blueTowerTop[i] != null)
            {
                Debug.Log(blueTowerMid[i].name + "is destroyed!");
                blueTowerMid[i].GetComponent<Health>().Damage(999999999999999999);
                break;
            }
        }
    }

    public void destroyBluefountainDefense()
    {
        for (int i = 0; i < redTowerfountain.Length; i++)
        {
            if (blueTowerfountain[i] != null)
            {
                Debug.Log(blueTowerfountain[i].name + "is destroyed!");
                Destroy(blueTowerfountain[i].gameObject);
            }

            else
            {
                Debug.Log("Fountain Tower Already Destroyed");
            }

        }
    }

    public void destroyRedfountainDefense()
    {
        for (int i = 0; i < redTowerfountain.Length; i++)
        {
            if (redTowerfountain[i] != null)
            {
                Debug.Log(redTowerfountain[i].name + "is destroyed!");
                Destroy(redTowerfountain[i].gameObject);
            }

            else
            {
                Debug.Log("Fountain Tower Already Destroyed");
            }

        }
    }

    //note: realised I could just assign the item to a ID#
    public void DestroyRedBarrack(int barrackID)
    {
        if (redBarracks[barrackID].gameObject != null)
        {
            Debug.Log(redBarracks[barrackID].name + "is destroyed!");
            Destroy(redBarracks[barrackID].gameObject);
        }
        else
        {
            Debug.Log("Barracks Already Destroyed");
        }
    }
    public void DestroyBlueBarrack(int barrackID)
    {
        if (blueBarracks[barrackID].gameObject != null)
        {
            Debug.Log(blueBarracks[barrackID].name + "is destroyed!");
            Destroy(blueBarracks[barrackID].gameObject);
        }
        else
        {
            Debug.Log("Barracks Already Destroyed");
        }
    }

}
